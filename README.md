# Entities Frameworks: Updates

## What is this?

This is the Releases and Updates Repository for all Frameworks hosted within the `entities` GitLab, like entityXML or GND-Marc.
Each subcollection represents a framework which contains several releases and an update site XML. 

## Frameworks hosted
- entityXML: https://gitlab.gwdg.de/entities/updates/-/raw/main/entityxml/updates.xml
- GND Marc: https://gitlab.gwdg.de/entities/updates/-/raw/main/gnd-marc/updates.xml


## HOWTO: Install frameworks as oXygen Add-Ons

Installing a framework as an add-on has the advantage that it automatically checks for updates. Accordingly, the installed frameworks always remain up-to-date and do not have to be updated manually.

How to install a framework as an add-on is described in the [official oXygen documentation](https://www.oxygenxml.com/doc/versions/26.0/ug-editor/topics/installing-and-updating-add-ons.html).

### Example: Installing entityXML as Add-On
To install entityXML as an add-on, we need the URL of the updates.xml configuration file. To do this, we navigate to the collection `entityxml` and click on `updates.xml`. We need the URL of the raw file; we can find it directly above the file in the right screen area

- `https://gitlab.gwdg.de/entities/updates/-/raw/main/entityxml/updates.xml`

Now in oXygen navigate to `Hilfe>Neue Add-Ons installieren` and copy the URL into the field `Add-Ons zeigen von`.

Now you can select the Add-Ons you like to install. From now on oXygen is checking for Updates on every startup.

## Questions?
<sikora@sub.uni-goettingen.de>